# <div align="center"> API Login Form </div>

<div align="center">
<p>🚧 It is in Development 🚧</p>
</br>
<p></p>
</div>

## Technologies used in the project

- Typescript
- NodeJS
- Express
- Typeorm
- Jest
- PostgreSQL

## Technologies used outside the project

- Docker
- insominia
- Beekeeper

## How to run the project

- Clone this repository

```shell
git clone https://github.com/guilhermelinosx/api-login-form.git
```

- Create a PostgreSQL Container using Docker

```shell
docker container create --name api-login-form -e POSTGRES_PASSWORD=admin -p 5432:5432 postgres
```

- Start the Container

```shell
docker container start api-login-form
```

- Stop the Container

```shell
docker container stop api-login-form
```

- Start the Application in Development

```shell
yarn dev
```

- To stop the Application click CTRL+C in your terminal
