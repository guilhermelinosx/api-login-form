import { UserRouter } from '@src/modules/domain/routers/UserRouter'
import { Router } from 'express'

export const router = Router()

router.use('/api/login', UserRouter)
