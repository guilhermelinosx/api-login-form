import { database } from '../database'
import { app } from './app'

database
	.initialize()
	.then(() => {
		app.listen(process.env.PORT, () => {
			console.info(`Server listening on: http://localhost:${process.env.PORT as string} 🚀`)
		})
	})
	.catch(err => {
		console.info(err)
	})
