import 'dotenv/config'
import 'express-async-errors'
import 'reflect-metadata'
import { errors } from 'celebrate'
import cors from 'cors'
import express, { Request, Response } from 'express'
import { router } from '../router'
import { InternalAppError } from '../utils/errors/internalAppError'

export const app = express()

app.use(express.json())
app.use(router)
app.use(cors())

app.use(errors())
app.use((err: Error, req: Request, res: Response) => {
	if (err instanceof InternalAppError) {
		return res.status(400).json({
			status: 'Error',
			message: err.message
		})
	}

	return res.status(500).json({
		status: 'Error',
		message: 'Internal server error'
	})
})
