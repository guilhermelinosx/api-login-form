import { DataSource } from 'typeorm'
import { User } from './entities/User'
import { UserToken } from './entities/UserToken'
import { User1663951958640 } from './migrations/1663951958640-User'
import { UserTokens1664193700744 } from './migrations/1664193700744-UserTokens'

export const database = new DataSource({
	type: 'postgres',
	host: 'localhost',
	port: 5432,
	username: 'postgres',
	password: 'admin',
	database: 'postgres',
	synchronize: true,
	migrations: [User1663951958640, UserTokens1664193700744],
	entities: [User, UserToken]
})
