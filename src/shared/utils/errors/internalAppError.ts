export class InternalAppError extends Error {
	constructor(public message: string, protected code: number = 400) {
		super(message)
		this.message = message
		this.code = code
	}
}
