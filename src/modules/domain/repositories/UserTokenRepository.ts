import { database } from '@src/shared/database'
import { UserToken } from '@src/shared/database/entities/UserToken'

export const UserTokenRepository = database.getRepository(UserToken).extend({
	async findByToken(token: string): Promise<UserToken | null> {
		const user = await this.findOneBy({ token })
		return user
	},

	async generateToken(user_id: string): Promise<UserToken | null> {
		const user = this.create({ user_id })
		await this.save(user)
		return user
	}
})
