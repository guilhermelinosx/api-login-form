export interface IUserToken {
	readonly id: string
	readonly token: string
	readonly user_id: string
	readonly created_at?: Date
	readonly updated_at?: Date
}
