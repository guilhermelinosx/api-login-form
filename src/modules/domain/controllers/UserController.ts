import { ForgotPasswordService } from '@src/modules/services/ForgotPasswordService'
import { ResetPasswordService } from '@src/modules/services/ResetPasswordService'
import { SignInService } from '@src/modules/services/SignInService'
import { SignUpService } from '@src/modules/services/SignUpService'
import { Request, Response } from 'express'

export class UserController {
	public async signup(req: Request, res: Response): Promise<Response> {
		const { name, email, password } = req.body
		const signUpService = new SignUpService()
		await signUpService.execute({ name, email, password })
		try {
			return res.status(201).json({})
		} catch (err) {
			return res.status(400).json(err)
		}
	}

	public async signin(req: Request, res: Response): Promise<Response> {
		const { email, password } = req.body
		const signInService = new SignInService()
		const user = await signInService.execute({ email, password })
		try {
			return res.status(204).json(user.token)
		} catch (err) {
			return res.status(400).json(err)
		}
	}

	public async forgotPassword(req: Request, res: Response): Promise<Response | null> {
		const { email } = req.body
		const forgotPasswordService = new ForgotPasswordService()
		const user = await forgotPasswordService.execute({ email })
		try {
			return res.status(204).json(user)
		} catch (err) {
			return res.status(400).json(err)
		}
	}

	public async resetPassword(req: Request, res: Response): Promise<Response> {
		const { token, password } = req.body
		const resetPasswordService = new ResetPasswordService()
		await resetPasswordService.execute({ token, password })
		try {
			return res.status(204).json({})
		} catch (err) {
			return res.status(400).json(err)
		}
	}
}
