import { InternalAppError } from '@src/shared/utils/errors/internalAppError'
import { UserRepository } from '../domain/repositories/UsersRepository'
import { UserTokenRepository } from '../domain/repositories/UserTokenRepository'

interface IRequest {
	email: string
}

export class ForgotPasswordService {
	constructor(
		private readonly userRepository = UserRepository,
		private readonly userTokenRepository = UserTokenRepository
	) {}

	public async execute({ email }: IRequest): Promise<void> {
		const user = await this.userRepository.findByEmail(email)
		if (!user) {
			throw new InternalAppError('User does not exists.')
		}

		await this.userTokenRepository.generateToken(user.id)
	}
}
