import { InternalAppError } from '@src/shared/utils/errors/internalAppError'
import { compare } from 'bcryptjs'
import { sign } from 'jsonwebtoken'
import { IUser } from '../domain/models/IUser'
import { UserRepository } from '../domain/repositories/UsersRepository'

interface IRequest {
	email: string
	password: string
}

interface IResponse {
	user: IUser
	token: string
}

export class SignInService {
	constructor(private readonly userRepository = UserRepository) {}

	public async execute({ email, password }: IRequest): Promise<IResponse> {
		const user = await this.userRepository.findByEmail(email)

		if (!user || !(await compare(password, user.password))) {
			throw new InternalAppError('Incorrect email/password combination.')
		}

		const token = sign({}, process.env.JWT_SECRET as string, {
			subject: user.id,
			expiresIn: process.env.JWT_EXPIRESIN as string
		})

		return { user, token }
	}
}
