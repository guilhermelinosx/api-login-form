import { InternalAppError } from '@src/shared/utils/errors/internalAppError'
import { hash } from 'bcryptjs'
import { IUser } from '../domain/models/IUser'
import { UserRepository } from '../domain/repositories/UsersRepository'

interface IRequest {
	name: string
	email: string
	password: string
}

export class SignUpService {
	constructor(private readonly userRepository = UserRepository) {}

	public async execute({ name, email, password }: IRequest): Promise<IUser> {
		const userEmail = await this.userRepository.findByEmail(email)
		if (userEmail !== null) {
			throw new InternalAppError('Email address already used.')
		}

		const user = this.userRepository.create({
			name,
			email,
			password: await hash(password, 8)
		})

		await this.userRepository.save(user)

		return user
	}
}
