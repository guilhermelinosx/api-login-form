import { UserRepository } from '../domain/repositories/UsersRepository'
import { isAfter, addHours } from 'date-fns'
import { hash } from 'bcryptjs'
import { UserTokenRepository } from '../domain/repositories/UserTokenRepository'
import { InternalAppError } from '@src/shared/utils/errors/internalAppError'

interface IRequest {
	token: string
	password: string
}

export class ResetPasswordService {
	constructor(
		private readonly userRepository = UserRepository,
		private readonly userTokenRepository = UserTokenRepository
	) {}

	public async execute({ token, password }: IRequest): Promise<void> {
		const userToken = await this.userTokenRepository.findByToken(token)

		if (!userToken) {
			throw new InternalAppError('User token does not exists.')
		}

		const user = await this.userRepository.findById(userToken.user_id)
		if (!user) {
			throw new InternalAppError('User does not exists.')
		}

		const tokenCreatedAt = userToken.created_at
		const compareDate = addHours(tokenCreatedAt, 2)
		if (isAfter(Date.now(), compareDate)) {
			throw new Error('Token expired.')
		}

		user.password = await hash(password, 8)

		await this.userRepository.save(user)
	}
}
